<?php
function pattern_count($text, $pattern) {
    $text_length = strlen($text);
    $pattern_length = strlen($pattern);
    $count = 0;

    if ($pattern_length == 0 || $pattern_length > $text_length) {
        return 0;
    }

    for ($i = 0; $i <= $text_length - $pattern_length; $i++) {
        if (substr($text, $i, $pattern_length) == $pattern) {
            $count++;
        }
    }

    return $count;
}

$result = null;

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $text = $_POST['text'];
    $pattern = $_POST['pattern'];

    $result = pattern_count($text, $pattern);
}
?>

<!DOCTYPE html>
<html lang="id">
<head>
    <meta charset="UTF-8">
    <title>Hitung Pola dalam Teks</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            background-color: #f2f2f2;
            margin: 0;
            padding: 20px;
        }

        h2 {
            color: #333;
            text-align : center;
            padding-bottom : 30px;
        }

        form {
            background-color: #fff;
            padding: 20px;
            border: 1px solid #ddd;
            border-radius: 5px;
            max-width: 400px;
            margin: auto;
        }

        label {
            display: block;
            margin-bottom: 10px;
        }

        input[type="text"] {
            width: 100%;
            padding: 8px;
            margin-bottom: 10px;
            border: 1px solid #ccc;
            border-radius: 4px;
        }

        input[type="submit"] {
            padding: 10px 20px;
            background-color: #4CAF50;
            color: white;
            border: none;
            border-radius: 5px;
            cursor: pointer;
            transition: background-color 0.3s;
        }

        input[type="submit"]:hover {
            background-color: #45a049;
        }

        .result {
            text-align: center;
            margin-top: 20px;
            font-size: 18px;
            color: #333;
        }
    </style>
</head>
<body>

<h2>Hitung Pola dalam Teks</h2>
<form method="post">
    <label for="text">Teks:</label>
    <input type="text" id="text" name="text" required>
    
    <label for="pattern">Pola:</label>
    <input type="text" id="pattern" name="pattern" required>
    
    <input type="submit" value="Hitung">
</form>

<?php if ($result !== null): ?>
    <div class="result">
        Jumlah pola '<?php echo htmlspecialchars($pattern); ?>' dalam teks '<?php echo htmlspecialchars($text); ?>' adalah: <?php echo $result; ?>
    </div>
<?php endif; ?>

</body>
</html>
