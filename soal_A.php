<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Urutkan Array</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            background-color: #f5f5f5;
            margin: 0;
            padding: 20px;
        }

        h1 {
            color: #333;
        }

        .container {
            max-width: 600px;
            margin: 0 auto;
            text-align: center;
        }

        .result {
            font-size: 16px;
            color: #444;
            background: #fff;
            padding: 10px;
            border: 1px solid #ddd;
            border-radius: 5px;
            margin-top: 10px;
        }

        .sort-button {
            padding: 10px 20px;
            font-size: 16px;
            background-color: #007BFF;
            color: #fff;
            border: none;
            border-radius: 5px;
            cursor: pointer;
            transition: background-color 0.3s;
        }

        .sort-button:hover {
            background-color: #0056b3;
        }
    </style>
    <script>
        function sortArray() {
            let arr = [12, 9, 30, "A", "M", 99, 82, "J", "N", "B"];

            let stringPart = arr.filter(item => typeof item === 'string');
            let numberPart = arr.filter(item => typeof item === 'number');

            stringPart.sort();

            numberPart.sort((a, b) => a - b);

            let sortedArray = [...stringPart, ...numberPart];

            document.getElementById('result').innerText = JSON.stringify(sortedArray);
        }
    </script>
</head>
<body>
    <div class="container">
        <h1>Hasil Pengurutan Array</h1>
        <button class="sort-button" onclick="sortArray()">Urutkan Array</button>
        <div class="result" id="result"></div>
    </div>
</body>
</html>
