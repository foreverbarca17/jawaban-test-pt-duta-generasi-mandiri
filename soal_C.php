<?php
function hitungHuruf($input) {
    $jumlahHuruf = [];

    for ($i = 0; $i < strlen($input); $i++) {
        $char = $input[$i];
        if (ctype_alpha($char)) { 
            if (isset($jumlahHuruf[$char])) {
                $jumlahHuruf[$char]++;
            } else {
                $jumlahHuruf[$char] = 1;
            }
        }
    }

    ksort($jumlahHuruf);

    return $jumlahHuruf;
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Hitung Huruf</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            background-color: #f4f4f4;
            margin: 0;
            padding: 0;
        }

        .container {
            max-width: 600px;
            margin: 50px auto;
            padding: 20px;
            background-color: white;
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.1);
        }

        h2 {
            text-align: center;
        }

        label {
            display: block;
            margin-bottom: 10px;
        }

        input {
            width: 100%;
            padding: 10px;
            margin-bottom: 20px;
        }

        button {
            padding: 10px 20px;
            background-color: #4CAF50;
            color: white;
            border: none;
            cursor: pointer;
        }

        button:hover {
            background-color: #45a049;
        }

        ul {
            list-style-type: none;
        }
    </style>
</head>
<body>
    <div class="container">
        <h2>Hitung Huruf dalam Kalimat</h2>
        <form method="post">
            <label for="kalimat">Masukkan Kalimat:</label>
            <input type="text" id="kalimat" name="kalimat" placeholder="Masukkan kalimat Anda di sini">
            <button type="submit">Hitung</button>
        </form>

        <?php
        if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['kalimat'])) {
            $input = $_POST['kalimat'];
            $hasil = hitungHuruf($input);

            echo "<h3>Hasil Penghitungan Huruf:</h3>";
            echo "<ul>";
            foreach ($hasil as $huruf => $jumlah) {
                echo "<li>$huruf: $jumlah</li>";
            }
            echo "</ul>";
        }
        ?>
    </div>
</body>
</html>
